
// Initialisation for contact page
function initMap(){
                    var mapOptions = {
                        scaleControl: true,
                        center: new google.maps.LatLng(28.645068,77.448608),
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
						
						     styles: [	

            {		featureType:'water',		stylers:[{color:'#B6F0A7'},{visibility:'on'}]	},

            {		featureType:'landscape',		stylers:[{color:'#f2f2f2'}]	},

            {		featureType:'road',		stylers:[{saturation:-100},{lightness:25}]	},

            {		featureType:'road.highway',		stylers:[{visibility:'simplified'}]	},

            {		featureType:'road.arterial',		elementType:'labels.icon',		stylers:[{visibility:'off'}]	},

            {		featureType:'administrative',		elementType:'labels.text.fill',		stylers:[{color:'#444444'}]	},

            {		featureType:'transit',		stylers:[{visibility:'off'}]	},

            {		featureType:'poi',		stylers:[{visibility:'off'}]	}]				

        

      
						
						
						
                    };
                    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
                    var marker = new google.maps.Marker({
                        map: map,
						"icon": 'images/map.png',
						
                         position: new google.maps.LatLng(28.645068,77.448608)
                    });

                   
                    var infowindow = new google.maps.InfoWindow();
                    infowindow.setContent('<b>Sirohi Agro Food Pvt. Ltd.</b>');
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map, marker);
                    });

}
// <=== end contact page



